<?php


namespace App\Utils\Product\Factory;


use App\Entity\Field;
use App\Utils\Product\Field\FieldBool;
use App\Utils\Product\Translation\Locale;

class FieldBoolFactory
{
    public static function create(Locale $locales, Field $_field, $title)
    {
        $field = new FieldBool();
        $field->setId($_field->getId());
        $field->setInternalName($_field->getName());
        $field->setDefaultValue($_field->getDefaultValue());
        $field->setUnit($_field->getId());
        $field->setTitle(TranslationFactory::create($locales, $title));

        return $field;
    }
}