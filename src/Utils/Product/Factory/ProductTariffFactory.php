<?php


namespace App\Utils\Product\Factory;


use App\Utils\Product\ProductTariff;
use App\Utils\Product\Translation\Locale;

class ProductTariffFactory
{
    /**
     * @param Locale $locales
     * @param string $internalName
     * @param array $titleTranslation
     * @param array $descriptionTranslation
     * @return ProductTariff
     */
    public static function create(
        Locale $locales,
        string $internalName,
        array $titleTranslation,
        array $descriptionTranslation
    ) {
        $product = new ProductTariff();
        $product->setInternalName($internalName);
        $product->setTitle(TranslationFactory::create($locales, $titleTranslation));
        $product->setDescription(TranslationFactory::create($locales, $descriptionTranslation));
        return $product;
    }
}