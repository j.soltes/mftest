<?php


namespace App\Utils\Product\Factory;


use App\Entity\Field;
use App\Utils\Product\Field\FieldSelect;
use App\Utils\Product\Translation\Locale;

class FieldSelectFactory
{
    public static function create(Locale $locales, Field $_field, $title, $options)
    {
        $field = new FieldSelect();
        $field->setId($_field->getId());
        $field->setInternalName($_field->getName());
        $field->setDefaultValue($_field->getDefaultValue());
        $field->setUnit($_field->getId());
        $field->setTitle(TranslationFactory::create($locales, $title));
        $field->setOptions(OptionFactory::create($locales, $options));
        return $field;
    }
}