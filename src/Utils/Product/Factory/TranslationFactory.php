<?php


namespace App\Utils\Product\Factory;


use App\Utils\Product\Translation\Locale;
use App\Utils\Product\Translation\Translation;
use InvalidArgumentException;

class TranslationFactory
{
    /**
     * @param Locale $locales
     * @param array $translationData
     * @return Translation
     * @throws InvalidArgumentException
     */
    public static function create(Locale $locales, $translationData)
    {
        $translation = new Translation($locales);
        foreach ($translationData as $locale => $text) {
            $translation->addTranslation($locale, $text);
        }
        return $translation;
    }
}