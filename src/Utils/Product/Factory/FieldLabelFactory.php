<?php


namespace App\Utils\Product\Factory;


use App\Entity\Field;
use App\Utils\Product\Field\FieldLabel;
use App\Utils\Product\Translation\Locale;

class FieldLabelFactory
{
    public static function create(Locale $locales, Field $_field, $title, $description)
    {
        $field = new FieldLabel();
        $field->setId($_field->getId());
        $field->setInternalName($_field->getName());
        $field->setDefaultValue($_field->getDefaultValue());
        $field->setUnit($_field->getId());
        $field->setTitle(TranslationFactory::create($locales, $title));
        $field->setDescription(TranslationFactory::create($locales, $description));

        return $field;
    }
}