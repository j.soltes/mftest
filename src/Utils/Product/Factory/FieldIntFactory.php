<?php


namespace App\Utils\Product\Factory;


use App\Entity\Field;
use App\Utils\Product\Field\FieldInt;
use App\Utils\Product\Translation\Locale;

class FieldIntFactory
{
    public static function create(Locale $locales, Field $_field, $title, $description)
    {
        $field = new FieldInt();
        $field->setId($_field->getId());
        $field->setInternalName($_field->getName());
        $field->setDefaultValue($_field->getDefaultValue());
        $field->setUnit($_field->getId());
        $field->setTitle(TranslationFactory::create($locales, $title));
        $field->setDescription(TranslationFactory::create($locales, $description));

        return $field;
    }
}