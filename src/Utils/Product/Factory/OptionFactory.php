<?php


namespace App\Utils\Product\Factory;


use App\Utils\Product\Field\Options\Options;
use App\Utils\Product\Translation\Locale;

class OptionFactory
{
    public static function create(Locale $locales, $data)
    {
        $options = new Options();
        foreach ($data as $key => $translation) {
            $options->addOption($key, TranslationFactory::create($locales, $translation));
        }
        return $options;
    }
}