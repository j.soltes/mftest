<?php


namespace App\Utils\Product\Factory;


use App\Utils\Product\Parameter;

class ParameterFactory
{
    public static function create($field, $value)
    {
        $parameter = new Parameter($field);
        $parameter->setValue($value);
        return $parameter;
    }
}