<?php


namespace App\Utils\Product\Factory;


use App\Utils\Product\ProductDevice;

use App\Utils\Product\Translation\Locale;

class ProductDeviceFactory
{
    /**
     * @param Locale $locales
     * @param string $internalName
     * @param array $titleTranslation
     * @param array $descriptionTranslation
     * @return ProductDevice
     */
    public static function create(
        Locale $locales,
        string $internalName,
        array $titleTranslation,
        array $descriptionTranslation
    ) {
        $product = new ProductDevice();
        $product->setInternalName($internalName);
        $product->setTitle(TranslationFactory::create($locales, $titleTranslation));
        $product->setDescription(TranslationFactory::create($locales, $descriptionTranslation));
        return $product;
    }
}