<?php


namespace App\Utils\Product;


class ProductTariff extends Product
{
    const TYPE = 'tariff';

    /**
     * ProductTariff constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->type = self::TYPE;
    }


}