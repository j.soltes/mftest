<?php


namespace App\Utils\Product;


use App\Utils\Product\Field\Field;

class Parameter
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $value;
    /**
     * @var Field
     */
    private $field;

    /**
     * Parameter constructor.
     * @param Field $field
     */
    public function __construct(Field $field)
    {
        $this->field = $field;
    }


    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|array|bool|int|float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string|array|bool|int|float $value
     */
    public function setValue($value): void
    {
        $this->field->validate($value);
        $this->value = $value;
    }

    /**
     * @return Field
     */
    public function getField(): Field
    {
        return $this->field;
    }

    /**
     * @param Field $field
     */
    public function setField(Field $field): void
    {
        $this->field = $field;
    }


}