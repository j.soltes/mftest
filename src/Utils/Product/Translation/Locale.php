<?php

namespace App\Utils\Product\Translation;


class Locale
{
    private $locales;

    /**
     * @return mixed
     */
    public function getLocales()
    {
        return $this->locales;
    }

    public function addLocale(string $locale)
    {
        $this->locales[] = $locale;
    }

    public function removeLocale(string $locale)
    {
        $this->locales[] = array_diff($this->locales[], [$locale]);
    }
}