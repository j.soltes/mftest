<?php

namespace App\Utils\Product\Translation;

use InvalidArgumentException;

class Translation
{
    /**
     * @var Locale
     */
    private $locales;
    /**
     * @var array
     */
    private $translations;

    /**
     * Translation constructor.
     * @param Locale $locales
     */
    public function __construct(Locale $locales)
    {
        $this->locales = $locales;
    }

    /**
     * @param string $locale
     * @param string $text
     * @return Translation
     * @throws InvalidArgumentException
     */
    public function addTranslation(string $locale, string $text): self
    {
        if (!in_array($locale, $this->locales->getLocales())) {
            throw new InvalidArgumentException('Locale doesnt exist');
        }

        $this->translations[$locale] = $text;
        return $this;
    }

    /**
     * @param string $locale
     */
    public function removeTranslation(string $locale)
    {
        if (!in_array($locale, $this->locales->getLocales())) {
            throw new InvalidArgumentException('Locale doesnt exist');
        }

        unset($this->translations[$locale]);
    }

    /**
     * @param string $locale
     * @return string
     */
    public function getTranslation(string $locale): string
    {
        if (!in_array($locale, $this->locales->getLocales())) {
            throw new InvalidArgumentException('Locale doesnt exist');
        }

        return $this->translations[$locale];
    }

    /**
     * @return array
     */
    public function getTranslations()
    {
        return $this->translations;
    }

}