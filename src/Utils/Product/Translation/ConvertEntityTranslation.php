<?php


namespace App\Utils\Product\Translation;


class ConvertEntityTranslation
{
    public static function convertTitle($translations)
    {
        $titleTranslations = [];
        foreach ($translations as $translation) {
            $titleTranslations[$translation->getLanguage()->getCode()] = $translation->getTitle();
        }
        return $titleTranslations;
    }

    public static function convertDescription($translations)
    {
        $descriptionTranslations = [];
        foreach ($translations as $translation) {
            $descriptionTranslations[$translation->getLanguage()->getCode()] = $translation->getDescription();
        }
        return $descriptionTranslations;
    }

    public static function convertFieldOption($options)
    {
        $optionsTranslation = [];
        foreach ($options as $option) {
            $translation = [];
            foreach ($option->getTranslations() as $optionTranslation) {
                $translation[$optionTranslation->getLanguage()->getCode()] = $optionTranslation->getTranslation();
            }
            $optionsTranslation[$option->getId()] = $translation;
        }
        return $optionsTranslation;
    }
}