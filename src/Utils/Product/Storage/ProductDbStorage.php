<?php


namespace App\Utils\Product\Storage;


use App\Entity\Language;
use App\Entity\Product as ProductDb;
use App\Entity\ProductParameter;
use App\Entity\ProductTranslation;
use App\Entity\ProductType;
use App\Repository\FieldRepository;
use App\Repository\LanguageRepository;
use App\Repository\ProductParameterRepository;
use App\Repository\ProductRepository;
use App\Repository\ProductTranslationRepository;
use App\Repository\ProductTypeRepository;
use App\Utils\Product\Parameter;
use App\Utils\Product\ProductInterface;
use Exception;
use InvalidArgumentException;

class ProductDbStorage
{

    /**
     * @var ProductInterface
     */
    private $product;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var ProductTranslationRepository
     */
    private $translationRepository;
    /**
     * @var ProductTypeRepository
     */
    private $productTypeRepository;
    /**
     * @var FieldRepository
     */
    private $fieldRepository;
    /**
     * @var ProductParameterRepository
     */
    private $parameterRepository;
    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * ProductDbStorage constructor.
     * @param ProductInterface $product
     * @param ProductRepository $productRepository
     * @param ProductTranslationRepository $translationRepository
     * @param ProductTypeRepository $productTypeRepository
     * @param FieldRepository $fieldRepository
     * @param ProductParameterRepository $parameterRepository
     * @param LanguageRepository $languageRepository
     */
    public function __construct(
        ProductInterface $product,
        ProductRepository $productRepository,
        ProductTranslationRepository $translationRepository,
        ProductTypeRepository $productTypeRepository,
        FieldRepository $fieldRepository,
        ProductParameterRepository $parameterRepository,
        LanguageRepository $languageRepository
    ) {

        $this->product = $product;
        $this->productRepository = $productRepository;
        $this->translationRepository = $translationRepository;
        $this->productTypeRepository = $productTypeRepository;
        $this->fieldRepository = $fieldRepository;
        $this->parameterRepository = $parameterRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @throws Exception
     */
    public function save()
    {
        $productEntity = $this->getProductEntity();
        $translationsEntities = $this->buildTranslationsEntities();

        foreach ($translationsEntities as $translationEntity) {
            $productEntity->addTranslation($translationEntity);
        }
        $productEntity->setType($this->getProductType());
        $productEntity->setInternalName($this->product->getInternalName());

        $parameters = $this->product->getParameters();
        foreach ($parameters as $parameter) {
            /**
             * @var Parameter $parameter
             */
            $parameterEntity = $this->getParameterEntity($parameter);

            $fieldEntity = $this->getFieldEntity($parameter);
            $parameterEntity->setField($fieldEntity);
            if (is_array($parameter->getValue())) {
                $parameterEntity->setValue($parameter->getValue());
            } else {
                $parameterEntity->setValue([$parameter->getValue()]);
            }
            $productEntity->addParameter($parameterEntity);
        }

        $this->productRepository->save($productEntity);
    }


    /**
     * @return ProductDb
     */
    private function getProductEntity()
    {
        if ($this->product->getId() > 0) {
            return $this->productRepository->find($this->product->getId());
        }
        return new ProductDb();
    }

    /**
     * @return array
     */
    private function buildTranslationsArray()
    {
        $translations = [];
        $titles = $this->product->getTitle()->getTranslations();
        $descriptions = $this->product->getDescription()->getTranslations();
        //$translation = $titles;
        foreach ($titles as $key => $title) {
            $translations[$key]['title'] = $title;
        }

        foreach ($descriptions as $key => $description) {
            $translations[$key]['description'] = $description;
        }
        return $translations;
    }

    /**
     * @return array
     */
    private function buildTranslationsEntities()
    {
        $translationEntities = [];
        $translations = $this->buildTranslationsArray();
        foreach ($translations as $language => $translation) {
            $translationEntity = $this->getTranslationEntity($language);
            $translationEntity->setLanguage($this->getLanguageEntity($language));
            $translationEntity->setTitle($translation['title']);
            $translationEntity->setDescription($translation['description']);
            $translationEntities[] = $translationEntity;
        }
        return $translationEntities;
    }

    /**
     * @param $language
     * @return ProductTranslation|null
     */
    private function getTranslationEntity($language)
    {
        $translationEntity = $this->translationRepository->findOneBy([
            'language' => $this->languageRepository->findOneBy(['code' => $language])->getId(),
            'product' => $this->product->getId()
        ]);
        if ($translationEntity == null) {
            return new ProductTranslation();
        }
        return $translationEntity;
    }

    /**
     * @return ProductType
     * @throws InvalidArgumentException
     */
    private function getProductType(): ProductType
    {
        $productType = $this->productTypeRepository->findOneBy(['code' => $this->product->getType()]);
        if ($productType == null) {
            throw new InvalidArgumentException('Product type doesnt exist');
        }
        return $productType;
    }

    /**
     * @param Parameter $parameter
     * @return \App\Entity\ProductParameter|null
     */
    private function getParameterEntity(Parameter $parameter)
    {
        if ($parameter->getId() > 0) {
            $parameterEntity = $this->parameterRepository->find($parameter->getId());
            return $parameterEntity;
        }
        $parameterEntity = new ProductParameter();
        return $parameterEntity;
    }

    /**
     * @param Parameter $parameter
     * @return \App\Entity\Field|null
     */
    private function getFieldEntity(Parameter $parameter)
    {
        $fieldEntity = $this->fieldRepository->find($parameter->getField()->getId());
        if ($fieldEntity == null) {
            throw new InvalidArgumentException("Field doesn't exist");
        }
        return $fieldEntity;
    }

    /**
     * @param $language
     * @return Language|null
     * @throws InvalidArgumentException
     */
    private function getLanguageEntity($language)
    {
        $languageEntity = $this->languageRepository->findOneBy(['code' => $language]);
        if ($languageEntity == null) {
            throw new InvalidArgumentException('Language doesnt exist');
        }
        return $languageEntity;
    }
}