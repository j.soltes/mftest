<?php


namespace App\Utils\Product\Field;


use InvalidArgumentException;

class FieldString extends Field
{
    /**
     * @var string
     */
    private $type = 'string';

    public function validate($value): void
    {
        if (!is_string($value)) {
            throw new InvalidArgumentException("Invalid value type");
        }
    }
}