<?php


namespace App\Utils\Product\Field;


use InvalidArgumentException;

class FieldFloat extends Field
{
    /**
     * @var string
     */
    private $type = 'float';

    public function validate($value): void
    {
        if (!is_float($value)) {
            throw new InvalidArgumentException("Invalid value type");
        }
    }
}