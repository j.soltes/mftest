<?php

namespace App\Utils\Product\Field;


use App\Utils\Product\Translation\Translation;

abstract class Field implements FieldInterface
{
    /**
     * @var string
     */
    const TYPE = '';

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var FieldInterface
     */
    private $parent;

    /**
     * @var string
     */
    private $internalName;

    /**
     * @var Translation
     */
    private $title;

    /**
     * @var Translation
     */
    private $description;

    /**
     * @var Translation
     */
    private $unit = '';

    private $defaultValue;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::TYPE;
    }

    /**
     * @return FieldInterface
     */
    public function getParent(): ?FieldInterface
    {
        return $this->parent;
    }

    /**
     * @param FieldInterface $parent
     */
    public function setParent(FieldInterface $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return string
     */
    public function getInternalName(): string
    {
        return $this->internalName;
    }

    /**
     * @param string $internalName
     */
    public function setInternalName(string $internalName): void
    {
        $this->internalName = $internalName;
    }

    /**
     * @return Translation
     */
    public function getTitle(): Translation
    {
        return $this->title;
    }

    /**
     * @param Translation $title
     */
    public function setTitle(Translation $title): void
    {
        $this->title = $title;
    }

    /**
     * @return Translation
     */
    public function getDescription(): Translation
    {
        return $this->description;
    }

    /**
     * @param Translation $description
     */
    public function setDescription(Translation $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getUnit(): string
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     */
    public function setUnit(string $unit): void
    {
        $this->unit = $unit;
    }

    /**
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @param mixed $defaultValue
     */
    public function setDefaultValue($defaultValue): void
    {
        $this->defaultValue = $defaultValue;
    }

    public function validate($value): void
    {
    }


}
