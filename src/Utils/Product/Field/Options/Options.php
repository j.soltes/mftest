<?php


namespace App\Utils\Product\Field\Options;


use App\Utils\Product\Translation\Translation;
use InvalidArgumentException;

class Options
{

    /**
     * @var array
     */
    private $options;

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param int $id
     * @param Translation $label
     * @return Options
     */
    public function addOption(int $id, Translation $label): self
    {
        $this->options[$id] = $label;
        return $this;
    }

    /**
     * @param int $id
     */
    public function removeOption(int $id): void
    {
        if (!array_key_exists($id, $this->options)) {
            throw new InvalidArgumentException("Option doesn't exist");
        }
        unset($this->options[$id]);
    }


}