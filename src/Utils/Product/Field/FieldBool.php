<?php


namespace App\Utils\Product\Field;


use InvalidArgumentException;

class FieldBool extends Field
{
    /**
     * @var string
     */
    private $type = 'bool';

    public function validate($value): void
    {
        if (!is_bool($value)) {
            throw new InvalidArgumentException("Invalid value type");
        }
    }
}