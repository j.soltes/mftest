<?php


namespace App\Utils\Product\Field;


use InvalidArgumentException;

class FieldLabel extends Field
{
    /**
     * @var string
     */
    private $type = 'label';

    public function validate($value): void
    {
        if (!is_string($value)) {
            throw new InvalidArgumentException("Invalid value type");
        }
    }
}