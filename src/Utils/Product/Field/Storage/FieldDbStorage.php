<?php


namespace App\Utils\Product\Field\Storage;


use App\Entity\Field as FieldDb;
use App\Entity\FieldTranslation;
use App\Entity\Language;
use App\Repository\FieldRepository;
use App\Repository\FieldTranslationRepository;
use App\Repository\LanguageRepository;
use App\Utils\Product\Field\FieldInterface;
use Exception;
use \InvalidArgumentException;

class FieldDbStorage
{
    /**
     * @var FieldInterface
     */
    private $field;

    /**
     * @var FieldRepository
     */
    private $repository;
    /**
     * @var FieldTranslationRepository
     */
    private $translationRepository;
    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * FieldStorage constructor.
     * @param FieldInterface $field
     * @param FieldRepository $repository
     * @param FieldTranslationRepository $translationRepository
     * @param LanguageRepository $languageRepository
     */
    public function __construct(
        FieldInterface $field,
        FieldRepository $repository,
        FieldTranslationRepository $translationRepository,
        LanguageRepository $languageRepository
    ) {
        $this->field = $field;
        $this->repository = $repository;
        $this->translationRepository = $translationRepository;
        $this->languageRepository = $languageRepository;
    }


    /**
     * @throws Exception
     */
    public function save()
    {
        $fieldEntity = $this->getFieldEntity();
        $translationsEntities = $this->buildTranslationsEntities();
        foreach ($translationsEntities as $translationEntity) {
            $fieldEntity->addTranslation($translationEntity);
        }
        $fieldEntity->setType($this->field->getType());
        $fieldEntity->setUnit($this->field->getUnit());
        $fieldEntity->setDefaultValue($this->field->getDefaultValue());
        $fieldEntity->setName($this->field->getInternalName());
        $fieldEntity->setParent($this->getParent());
        $this->repository->save($fieldEntity);
    }

    /**
     * @return FieldDb|null
     */
    private function getFieldEntity()
    {
        if ($this->field->getId() > 0) {
            return $this->repository->find($this->field->getId());
        }
        return new FieldDb();
    }

    /**
     * @return FieldDb|null
     */
    private function getParent()
    {
        if ($this->field->getParent() && $this->field->getParent()->getId() > 0) {
            return $this->repository->find($this->field->getParent()->getId());
        }
        return null;
    }

    /**
     * @return array
     */
    private function buildTranslationsArray()
    {
        $translations = [];
        $titles = $this->field->getTitle()->getTranslations();
        $descriptions = $this->field->getDescription()->getTranslations();
        //$translation = $titles;
        foreach ($titles as $key => $title) {
            $translations[$key]['title'] = $title;
        }

        foreach ($descriptions as $key => $description) {
            $translations[$key]['description'] = $description;
        }
        return $translations;
    }

    /**
     * @return array
     */
    private function buildTranslationsEntities()
    {
        $translations = $this->buildTranslationsArray();
        $translationEntities = [];
        foreach ($translations as $language => $translation) {
            $translationEntity = $this->getTranslationEntity($language);
            $translationEntity->setLanguage($this->getLanguageEntity($language));
            $translationEntity->setTitle($translation['title']);
            $translationEntity->setDescription($translation['description']);
            $translationEntities[] = $translationEntity;
        }
        return $translationEntities;
    }

    /**
     * @param $language
     * @return FieldTranslation|null
     */
    private function getTranslationEntity($language)
    {
        $translationEntity = $this->translationRepository->findOneBy([
            'language' => $this->languageRepository->findOneBy(['code' => $language])->getId(),
            'field' => $this->field->getId()
        ]);
        if ($translationEntity == null) {
            return new FieldTranslation();
        }
        return $translationEntity;
    }

    /**
     * @param $language
     * @return Language|null
     * @throws InvalidArgumentException
     */
    private function getLanguageEntity($language)
    {
        $languageEntity = $this->languageRepository->findOneBy(['code' => $language]);
        if ($languageEntity == null) {
            throw new InvalidArgumentException('Language doesnt exist');
        }
        return $languageEntity;
    }

}