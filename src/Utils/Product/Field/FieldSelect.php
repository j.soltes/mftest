<?php


namespace App\Utils\Product\Field;


use App\Utils\Product\Field\Options\Options;
use App\Utils\Product\Translation\Translation;
use InvalidArgumentException;

class FieldSelect extends Field
{
    /**
     * @var string
     */
    private $type = 'select';
    /**
     * @var Options
     */
    private $options;

    /**
     * @param int $id
     * @param Translation $label
     * @return FieldSelect
     */
    public function addOption(int $id, Translation $label): self
    {
        $this->options->addOption($id, $label);
        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options->getOptions();
    }

    /**
     * @param Options $options
     */
    public function setOptions(Options $options): void
    {
        $this->options = $options;
    }

    public function validate($value): void
    {
        if (!is_int($value)) {
            throw new InvalidArgumentException("Invalid value type");
        }
    }
}