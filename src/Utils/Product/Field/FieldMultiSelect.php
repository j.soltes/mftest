<?php


namespace App\Utils\Product\Field;


use InvalidArgumentException;

class FieldMultiSelect extends FieldSelect
{
    /**
     * @var string
     */
    private $type = 'multi_select';

    public function validate($value): void
    {
        if (!is_array($value)) {
            throw new InvalidArgumentException("Invalid value type");
        }
    }
}