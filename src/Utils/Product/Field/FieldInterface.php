<?php

namespace App\Utils\Product\Field;

use App\Utils\Product\Translation\Translation;
use InvalidArgumentException;

interface FieldInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param int $id
     */
    public function setId(int $id): void;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return FieldInterface
     */
    public function getParent(): ?FieldInterface;

    /**
     * @param FieldInterface $parent
     */
    public function setParent(FieldInterface $parent): void;

    /**
     * @return string
     */
    public function getInternalName(): string;

    /**
     * @param string $internalName
     */
    public function setInternalName(string $internalName): void;

    /**
     * @return Translation
     */
    public function getTitle(): Translation;

    /**
     * @param Translation $title
     */
    public function setTitle(Translation $title): void;

    /**
     * @return Translation
     */
    public function getDescription(): Translation;

    /**
     * @param Translation $description
     */
    public function setDescription(Translation $description): void;

    /**
     * @return string
     */
    public function getUnit(): string;

    /**
     * @param string $unit
     */
    public function setUnit(string $unit): void;

    /**
     * @return mixed
     */
    public function getDefaultValue();

    /**
     * @param mixed $defaultValue
     */
    public function setDefaultValue($defaultValue): void;

    /**
     * @param $value
     * @throws InvalidArgumentException
     */
    function validate($value): void;
}