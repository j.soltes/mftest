<?php


namespace App\Utils\Product\Field;


use InvalidArgumentException;

class FieldInt extends Field
{
    /**
     * @var string
     */
    private $type = 'int';

    public function validate($value): void
    {
        if (!is_int($value)) {
            throw new InvalidArgumentException("Invalid value type");
        }
    }
}