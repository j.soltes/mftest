<?php


namespace App\Utils\Product;


class ProductDevice extends Product
{
    const TYPE = 'device';

    /**
     * ProductTariff constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->type = self::TYPE;
    }
}