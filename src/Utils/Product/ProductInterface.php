<?php

namespace App\Utils\Product;

use App\Utils\Product\Translation\Translation;

interface ProductInterface
{
    /**
     * @return int
     */
    public function getId(): ?int;

    /**
     * @param int $id
     */
    public function setId(int $id): void;

    /**
     * @return string
     */
    public function getInternalName(): string;

    /**
     * @param string $internalName
     */
    public function setInternalName(string $internalName): void;

    /**
     * @return Translation
     */
    public function getTitle(): Translation;

    /**
     * @param Translation $translation
     */
    public function setTitle(Translation $translation): void;

    /**
     * @return Translation
     */
    public function getDescription(): Translation;

    /**
     * @param Translation $translation
     */
    public function setDescription(Translation $translation): void;


    /**
     * @return array
     */
    public function getParameters(): array;

    /**
     * @param Parameter $parameter
     */
    public function addParameter(Parameter $parameter): void;

    /**
     * @return string
     */
    public function getType(): string;
}