<?php


namespace App\Utils\Product;


class ProductCard extends Product
{
    const TYPE = 'card';

    /**
     * ProductTariff constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->type = self::TYPE;
    }
}