<?php

namespace App\Utils\Product;


use App\Utils\Product\Translation\Translation;

abstract class Product implements ProductInterface
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $internalName;

    /**
     * @var Translation
     */
    public $title;

    /**
     * @var Translation
     */
    private $description;

    /**
     * @var Parameter[]
     */
    private $parameters;
    /**
     * @var string
     */
    protected $type;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->type = '';
    }


    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getInternalName(): string
    {
        return $this->internalName;
    }

    /**
     * @param string $internalName
     */
    public function setInternalName(string $internalName): void
    {
        $this->internalName = $internalName;
    }

    /**
     * @return Translation
     */
    public function getTitle(): Translation
    {
        return $this->title;
    }

    /**
     * @param Translation $translation
     */
    public function setTitle(Translation $translation): void
    {
        $this->title = $translation;
    }

    /**
     * @return Translation
     */
    public function getDescription(): Translation
    {
        return $this->description;
    }

    /**
     * @param Translation $translation
     */
    public function setDescription(Translation $translation): void
    {
        $this->description = $translation;
    }


    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param Parameter $parameter
     */
    public function addParameter(Parameter $parameter): void
    {
        $this->parameters[] = $parameter;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}
