<?php

namespace App\Controller;

use App\Repository\FieldRepository;
use App\Repository\FieldTranslationRepository;
use App\Repository\LanguageRepository;
use App\Repository\ProductParameterRepository;
use App\Repository\ProductRepository;
use App\Repository\ProductTranslationRepository;
use App\Repository\ProductTypeRepository;
use App\Utils\Product\Factory\FieldBoolFactory;
use App\Utils\Product\Factory\FieldIntFactory;
use App\Utils\Product\Factory\FieldLabelFactory;
use App\Utils\Product\Factory\FieldMultiSelectFactory;
use App\Utils\Product\Factory\FieldSelectFactory;
use App\Utils\Product\Factory\ParameterFactory;
use App\Utils\Product\Factory\ProductDeviceFactory;
use App\Utils\Product\Factory\ProductTariffFactory;
use App\Utils\Product\Factory\TranslationFactory;
use App\Utils\Product\Field\FieldLabel;
use App\Utils\Product\Field\Storage\FieldDbStorage;
use App\Utils\Product\Storage\ProductDbStorage;
use App\Utils\Product\Translation\ConvertEntityTranslation;
use App\Utils\Product\Translation\Locale;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    /**
     * @var Locale
     */
    private $locale;

    public function __construct(Locale $locale)
    {
        $this->locale = $locale;
        $this->locale->addLocale('cs');
        $this->locale->addLocale('en');
    }

    /**
     * @Route("/", name="save_tariff")
     * @param ProductRepository $productRepository
     * @param ProductTranslationRepository $translationRepository
     * @param ProductTypeRepository $productTypeRepository
     * @param FieldRepository $fieldRepository
     * @param ProductParameterRepository $parameterRepository
     * @param LanguageRepository $languageRepository
     * @return Response
     * @throws Exception
     */
    public function newTariff(
        ProductRepository $productRepository,
        ProductTranslationRepository $translationRepository,
        ProductTypeRepository $productTypeRepository,
        FieldRepository $fieldRepository,
        ProductParameterRepository $parameterRepository,
        LanguageRepository $languageRepository
    ) {
        // load fields
        $tariffFieldBool = $fieldRepository->find(3);
        $tariffFieldBoolTitles = ConvertEntityTranslation::convertTitle($tariffFieldBool->getTranslations());

        $tariffFieldSelect = $fieldRepository->find(2);
        $tariffFieldSelectTitles = ConvertEntityTranslation::convertTitle($tariffFieldSelect->getTranslations());
        $tariffFieldSelectOptions = ConvertEntityTranslation::convertFieldOption($tariffFieldSelect->getFieldOptions());

        //create product
        $product = ProductTariffFactory::create(
            $this->locale,
            'Tariff',
            ['en' => 'Tariff 01', 'cs' => 'Tarif 01'],
            ['en' => 'Tariff description', 'cs' => 'Tarif popis']
        );

        //add parameters
        $field = FieldMultiSelectFactory::create($this->locale, $tariffFieldSelect, $tariffFieldSelectTitles,
            $tariffFieldSelectOptions);
        $parameter = ParameterFactory::create($field, [1, 2, 3]);
        $product->addParameter($parameter);

        $field = FieldBoolFactory::create($this->locale, $tariffFieldBool, $tariffFieldBoolTitles);
        $parameter = ParameterFactory::create($field, true);
        $product->addParameter($parameter);

        //save product
        $productStorage = new ProductDbStorage(
            $product,
            $productRepository,
            $translationRepository,
            $productTypeRepository,
            $fieldRepository,
            $parameterRepository,
            $languageRepository
        );
        $productStorage->save();

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'product' => $product,
        ]);
    }

    /**
     * @Route("/device", name="save_device")
     * @param ProductRepository $productRepository
     * @param ProductTranslationRepository $translationRepository
     * @param ProductTypeRepository $productTypeRepository
     * @param FieldRepository $fieldRepository
     * @param ProductParameterRepository $parameterRepository
     * @param LanguageRepository $languageRepository
     * @return Response
     * @throws Exception
     */
    public function newDevice(
        ProductRepository $productRepository,
        ProductTranslationRepository $translationRepository,
        ProductTypeRepository $productTypeRepository,
        FieldRepository $fieldRepository,
        ProductParameterRepository $parameterRepository,
        LanguageRepository $languageRepository
    ) {
        // load fields
        $fieldBoolLTE = $fieldRepository->find(9);
        $fieldBoolLTETitles = ConvertEntityTranslation::convertTitle($fieldBoolLTE->getTranslations());

        $fieldSelectOS = $fieldRepository->find(8);
        $fieldSelectOSTitles = ConvertEntityTranslation::convertTitle($fieldSelectOS->getTranslations());
        $fieldSelectOSOptions = ConvertEntityTranslation::convertFieldOption($fieldSelectOS->getFieldOptions());

        $fieldLabelSize = $fieldRepository->find(1);
        $fieldLabelSizeTitles = ConvertEntityTranslation::convertTitle($fieldLabelSize->getTranslations());
        $fieldLabelSizeDescriptions = ConvertEntityTranslation::convertDescription($fieldLabelSize->getTranslations());

        $fieldIntHeight = $fieldRepository->find(4);
        $fieldIntHeightTitles = ConvertEntityTranslation::convertTitle($fieldIntHeight->getTranslations());
        $fieldIntHeightDescriptions = ConvertEntityTranslation::convertDescription($fieldIntHeight->getTranslations());

        $fieldIntWidth = $fieldRepository->find(5);
        $fieldIntWidthTitles = ConvertEntityTranslation::convertTitle($fieldIntWidth->getTranslations());
        $fieldIntWidthDescriptions = ConvertEntityTranslation::convertDescription($fieldIntWidth->getTranslations());

        $fieldIntLength = $fieldRepository->find(6);
        $fieldIntLengthTitles = ConvertEntityTranslation::convertTitle($fieldIntLength->getTranslations());
        $fieldIntLengthDescriptions = ConvertEntityTranslation::convertDescription($fieldIntLength->getTranslations());

        //create product
        $product = ProductDeviceFactory::create(
            $this->locale,
            'Device 01',
            ['en' => 'Device 01', 'cs' => 'Zařízení 01'],
            ['en' => 'Device description', 'cs' => 'Zařízení popis']
        );

        //add parameters
        $field = FieldSelectFactory::create($this->locale, $fieldSelectOS, $fieldSelectOSTitles,
            $fieldSelectOSOptions);
        $parameter = ParameterFactory::create($field, 1);
        $product->addParameter($parameter);

        $field = FieldBoolFactory::create($this->locale, $fieldBoolLTE, $fieldBoolLTETitles);
        $parameter = ParameterFactory::create($field, true);
        $product->addParameter($parameter);

        $fieldLabel = FieldLabelFactory::create($this->locale, $fieldLabelSize, $fieldLabelSizeTitles,
            $fieldLabelSizeDescriptions);
        $parameter = ParameterFactory::create($fieldLabel, '');
        $product->addParameter($parameter);

        $field = FieldIntFactory::create($this->locale, $fieldIntHeight, $fieldIntHeightTitles,
            $fieldIntHeightDescriptions);
        $field->setParent($fieldLabel);
        $parameter = ParameterFactory::create($field, 150);
        $product->addParameter($parameter);

        $field = FieldIntFactory::create($this->locale, $fieldIntWidth, $fieldIntWidthTitles,
            $fieldIntWidthDescriptions);
        $field->setParent($fieldLabel);
        $parameter = ParameterFactory::create($field, 150);
        $product->addParameter($parameter);

        $field = FieldIntFactory::create($this->locale, $fieldIntLength, $fieldIntLengthTitles,
            $fieldIntLengthDescriptions);
        $field->setParent($fieldLabel);
        $parameter = ParameterFactory::create($field, 150);
        $product->addParameter($parameter);

        //save product
        $productStorage = new ProductDbStorage(
            $product,
            $productRepository,
            $translationRepository,
            $productTypeRepository,
            $fieldRepository,
            $parameterRepository,
            $languageRepository
        );
        $productStorage->save();

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'product' => $product,
        ]);
    }

    /**
     * @Route("/save-field", name="save_field")
     * @param FieldRepository $fieldRepository
     * @param FieldTranslationRepository $fieldTranslationRepository
     * @param LanguageRepository $languageRepository
     * @return Response
     * @throws Exception
     */
    public function saveField(
        FieldRepository $fieldRepository,
        FieldTranslationRepository $fieldTranslationRepository,
        LanguageRepository $languageRepository
    ) {
        $field = new FieldLabel();
        $field->setId(1);
        $field->setInternalName('Label rozmeru');
        $field->setTitle(TranslationFactory::create($this->locale, ['cs' => 'Rozměry', 'en' => 'Sizes']));
        $field->setDescription(TranslationFactory::create($this->locale, ['cs' => 'Popis', 'en' => 'Description']));

        $fieldStorage = new FieldDbStorage($field, $fieldRepository, $fieldTranslationRepository, $languageRepository);
        $fieldStorage->save();

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'product' => $field,
        ]);
    }
}
