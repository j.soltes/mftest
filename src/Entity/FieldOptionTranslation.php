<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FieldOptionTranslationRepository")
 */
class FieldOptionTranslation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language")
     */
    private $language;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FieldOption", inversedBy="translations")
     */
    private $fieldOption;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $translation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getFieldOption(): ?FieldOption
    {
        return $this->fieldOption;
    }

    public function setFieldOption(?FieldOption $fieldOption): self
    {
        $this->fieldOption = $fieldOption;

        return $this;
    }

    public function getTranslation(): ?string
    {
        return $this->translation;
    }

    public function setTranslation(string $translation): self
    {
        $this->translation = $translation;

        return $this;
    }
}
