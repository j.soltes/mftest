<?php

namespace App\Repository;

use App\Entity\FieldOptionTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FieldOptionTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method FieldOptionTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method FieldOptionTranslation[]    findAll()
 * @method FieldOptionTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FieldOptionTranslationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FieldOptionTranslation::class);
    }

    // /**
    //  * @return FieldOptionTranslation[] Returns an array of FieldOptionTranslation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FieldOptionTranslation
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
