<?php

namespace App\Repository;

use App\Entity\FieldTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FieldTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method FieldTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method FieldTranslation[]    findAll()
 * @method FieldTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FieldTranslationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FieldTranslation::class);
    }

    // /**
    //  * @return FieldTranslation[] Returns an array of FieldTranslation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FieldTranslation
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
