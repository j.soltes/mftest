<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Exception;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @param Product $product
     * @throws Exception
     */
    public function save(Product $product)
    {
        $this->_em->beginTransaction();
        //try {
        $this->_em->persist($product);
        $this->_em->flush();
        /*} catch (Exception $e) {
            $this->_em->rollback();
            throw new Exception('Error Product not saved');
        }*/
        $this->_em->commit();
    }
}
