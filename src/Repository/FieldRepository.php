<?php

namespace App\Repository;

use App\Entity\Field;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Exception;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Field|null find($id, $lockMode = null, $lockVersion = null)
 * @method Field|null findOneBy(array $criteria, array $orderBy = null)
 * @method Field[]    findAll()
 * @method Field[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FieldRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Field::class);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findById($id)
    {
        return $this->createQueryBuilder('f')
            ->select('f', 't')
            ->leftJoin('f.translations', 't')
            ->andWhere('f.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param Field $field
     * @throws Exception
     */
    public function save(Field $field)
    {
        $this->_em->beginTransaction();
        try {
            $this->_em->persist($field);
            $this->_em->flush();
        } catch (Exception $e) {
            $this->_em->rollback();
            throw new Exception('Error field not saved');
        }
        $this->_em->commit();
    }

}
