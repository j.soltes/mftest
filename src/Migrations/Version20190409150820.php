<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190409150820 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE field (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, unit VARCHAR(8) DEFAULT NULL, default_value VARCHAR(255) DEFAULT NULL, type VARCHAR(16) NOT NULL, INDEX IDX_5BF54558727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE field_option (id INT AUTO_INCREMENT NOT NULL, field_id INT DEFAULT NULL, INDEX IDX_70C28CB2443707B0 (field_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE field_option_translation (id INT AUTO_INCREMENT NOT NULL, language_id INT DEFAULT NULL, field_option_id INT DEFAULT NULL, translation VARCHAR(255) NOT NULL, INDEX IDX_83138AFB82F1BAF4 (language_id), INDEX IDX_83138AFB42C79BE5 (field_option_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE field_translation (id INT AUTO_INCREMENT NOT NULL, language_id INT DEFAULT NULL, field_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_71DEF6B882F1BAF4 (language_id), INDEX IDX_71DEF6B8443707B0 (field_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE language (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(2) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, internal_name VARCHAR(255) NOT NULL, INDEX IDX_D34A04ADC54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_parameter (id INT AUTO_INCREMENT NOT NULL, field_id INT DEFAULT NULL, product_id INT DEFAULT NULL, value LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_4437279D443707B0 (field_id), INDEX IDX_4437279D4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_translation (id INT AUTO_INCREMENT NOT NULL, language_id INT NOT NULL, product_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_1846DB7082F1BAF4 (language_id), INDEX IDX_1846DB704584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(32) NOT NULL, code VARCHAR(16) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE field ADD CONSTRAINT FK_5BF54558727ACA70 FOREIGN KEY (parent_id) REFERENCES field (id)');
        $this->addSql('ALTER TABLE field_option ADD CONSTRAINT FK_70C28CB2443707B0 FOREIGN KEY (field_id) REFERENCES field (id)');
        $this->addSql('ALTER TABLE field_option_translation ADD CONSTRAINT FK_83138AFB82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE field_option_translation ADD CONSTRAINT FK_83138AFB42C79BE5 FOREIGN KEY (field_option_id) REFERENCES field_option (id)');
        $this->addSql('ALTER TABLE field_translation ADD CONSTRAINT FK_71DEF6B882F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE field_translation ADD CONSTRAINT FK_71DEF6B8443707B0 FOREIGN KEY (field_id) REFERENCES field (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADC54C8C93 FOREIGN KEY (type_id) REFERENCES product_type (id)');
        $this->addSql('ALTER TABLE product_parameter ADD CONSTRAINT FK_4437279D443707B0 FOREIGN KEY (field_id) REFERENCES field (id)');
        $this->addSql('ALTER TABLE product_parameter ADD CONSTRAINT FK_4437279D4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_translation ADD CONSTRAINT FK_1846DB7082F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE product_translation ADD CONSTRAINT FK_1846DB704584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('INSERT INTO `language` (`id`, `code`, `name`) VALUES
(1, \'cs\', \'Česky\'),
(2, \'en\', \'Anglicky\');

        INSERT INTO `product_type` (`id`, `name`, `code`) VALUES
(1, \'Device\', \'device\'),
(2, \'Tariff\', \'tariff\'),
(3, \'Card\', \'card\');

INSERT INTO `field` (`id`, `parent_id`, `name`, `unit`, `default_value`, `type`) VALUES
(1, NULL, \'Label rozmeru\', \'\', NULL, \'label\'),
(2, NULL, \'Typ zákazníka\', NULL, NULL, \'select\'),
(3, NULL, \'pouze se smlouvou\', \'\', NULL, \'bool\'),
(4, 1, \'Výska\', \'mm\', \'100\', \'int\'),
(5, 1, \'Šířka\', \'mm\', \'100\', \'int\'),
(6, 1, \'Délka\', \'mm\', \'100\', \'int\'),
(7, NULL, \'Váha\', \'g\', \'100\', \'int\'),
(8, NULL, \'Operační systém\', NULL, NULL, \'select\'),
(9, NULL, \'podpora LTE\', NULL, NULL, \'bool\');

--
-- Vypisuji data pro tabulku `field_option`
--

INSERT INTO `field_option` (`id`, `field_id`) VALUES
(1, 2),
(2, 2),
(3, 2),
(4, 8),
(5, 8);

--
-- Vypisuji data pro tabulku `field_option_translation`
--

INSERT INTO `field_option_translation` (`id`, `language_id`, `field_option_id`, `translation`) VALUES
(1, 1, 1, \'spotřebitel\'),
(2, 2, 1, \'cosumer\'),
(3, 1, 2, \'Firemní\'),
(4, 2, 2, \'Business\'),
(5, 1, 3, \'Student\'),
(6, 2, 3, \'Student\'),
(7, 1, 4, \'Android\'),
(8, 2, 4, \'Android\'),
(9, 1, 5, \'IOS\'),
(10, 2, 5, \'IOS\');

--
-- Vypisuji data pro tabulku `field_translation`
--

INSERT INTO `field_translation` (`id`, `language_id`, `field_id`, `title`, `description`) VALUES
(1, 1, 3, \'Pouze se smlouvou\', \'\'),
(2, 2, 3, \'Only with the contract\', \'\'),
(3, 1, 1, \'Rozměry\', \'Popis\'),
(4, 2, 1, \'Sizes\', \'Description\'),
(5, 1, 2, \'Typ zakaznika\', \'\'),
(6, 2, 2, \'Customer type\', \'\'),
(7, 1, 6, \'Délka\', \'Délka\'),
(8, 2, 6, \'Length\', \'Length\'),
(9, 1, 4, \'Výška\', \'Výška\'),
(10, 2, 4, \'Height\', \'Height\'),
(11, 1, 5, \'Šířka\', \'Šířka\'),
(12, 2, 5, \'Width\', \'Width\'),
(13, 1, 7, \'Váha\', \'Váha\'),
(14, 2, 7, \'Weight\', \'Weight\'),
(15, 1, 8, \'Operační systém\', \'Operační systém\'),
(16, 2, 8, \'System\', \'System\'),
(17, 1, 9, \'Podpora LTE\', \'Podpora LTE\'),
(18, 2, 9, \'LTE support\', \'LTE support\');

--
-- Vypisuji data pro tabulku `language`
--

INSERT INTO `language` (`id`, `code`, `name`) VALUES
(1, \'cs\', \'Česky\'),
(2, \'en\', \'Anglicky\');

--
-- Vypisuji data pro tabulku `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
(\'20190409150820\', \'2019-04-09 15:08:30\');

--
-- Vypisuji data pro tabulku `product_type`
--

INSERT INTO `product_type` (`id`, `name`, `code`) VALUES
(1, \'Device\', \'device\'),
(2, \'Tariff\', \'tariff\'),
(3, \'Card\', \'card\');');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE field DROP FOREIGN KEY FK_5BF54558727ACA70');
        $this->addSql('ALTER TABLE field_option DROP FOREIGN KEY FK_70C28CB2443707B0');
        $this->addSql('ALTER TABLE field_translation DROP FOREIGN KEY FK_71DEF6B8443707B0');
        $this->addSql('ALTER TABLE product_parameter DROP FOREIGN KEY FK_4437279D443707B0');
        $this->addSql('ALTER TABLE field_option_translation DROP FOREIGN KEY FK_83138AFB42C79BE5');
        $this->addSql('ALTER TABLE field_option_translation DROP FOREIGN KEY FK_83138AFB82F1BAF4');
        $this->addSql('ALTER TABLE field_translation DROP FOREIGN KEY FK_71DEF6B882F1BAF4');
        $this->addSql('ALTER TABLE product_translation DROP FOREIGN KEY FK_1846DB7082F1BAF4');
        $this->addSql('ALTER TABLE product_parameter DROP FOREIGN KEY FK_4437279D4584665A');
        $this->addSql('ALTER TABLE product_translation DROP FOREIGN KEY FK_1846DB704584665A');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADC54C8C93');
        $this->addSql('DROP TABLE field');
        $this->addSql('DROP TABLE field_option');
        $this->addSql('DROP TABLE field_option_translation');
        $this->addSql('DROP TABLE field_translation');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_parameter');
        $this->addSql('DROP TABLE product_translation');
        $this->addSql('DROP TABLE product_type');
    }
}
